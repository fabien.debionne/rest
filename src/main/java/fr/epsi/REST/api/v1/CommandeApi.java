package fr.epsi.REST.api.v1;

import fr.epsi.REST.api.dto.CommandeDto;
import fr.epsi.REST.exception.UnknowRessourceException;
import fr.epsi.REST.mapper.CommandeMapper;
import fr.epsi.REST.service.CommandeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/commandes")
@CrossOrigin(value= {"*"}, allowedHeaders = {"*"})
public class CommandeApi {

    private CommandeService commandeService;

    private CommandeMapper commandeMapper;

    public  CommandeApi(CommandeService commandeService, CommandeMapper commandeMapper){
        this.commandeService = commandeService;
        this.commandeMapper =commandeMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Retourne la liste de totes les commandes ", responses = @ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = CommandeDto.class)))))
    public ResponseEntity<List<CommandeDto>> getAll(){
        return ResponseEntity
                .ok(commandeService.getAll().stream()
                        .map(commande -> this.commandeMapper.mapToDto(commande))
                        .collect(Collectors.toList()));
    }


    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "Création de la commande",
            responses = @ApiResponse(responseCode = "201", description = "Commande créé"))
    public ResponseEntity<CommandeDto> create(@RequestBody final CommandeDto commandeDto){
        CommandeDto newCommande = commandeMapper
                .mapToDto(commandeService
                        .create(commandeMapper.mapToModel(commandeDto)));
        return ResponseEntity.created(URI.create("/commandes/" + newCommande.getId())).body(newCommande);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Suppression de la commande", responses = @ApiResponse(responseCode = "204", description = "Commande supprimée"))
    public ResponseEntity<Void>delete(@PathVariable final Integer id){
        try {
            commandeService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (UnknowRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "commande non trouvée");
        }
    }

    @PutMapping(path = "/{id}",
            produces = { MediaType.APPLICATION_JSON_VALUE},
            consumes = { MediaType.APPLICATION_JSON_VALUE,
                    MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "mise à jour de la commande", responses = @ApiResponse(responseCode = "204", description = "Pas de contenu"))
    public ResponseEntity<Void> update(@PathVariable final Integer id, @RequestBody CommandeDto commandeDto) {
        try{
            commandeDto.setId(id);
            commandeService.update(commandeMapper.mapToModel(commandeDto));
            return ResponseEntity.noContent().build();
        } catch (UnknowRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "commande non trouvée");
        }
    }
}

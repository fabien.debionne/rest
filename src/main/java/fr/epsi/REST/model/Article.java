package fr.epsi.REST.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name= "articles")
public class Article {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;

private String designation;
private Integer quantite;
private Double prix;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "commande_id", nullable = true)
private Commande commande;

}

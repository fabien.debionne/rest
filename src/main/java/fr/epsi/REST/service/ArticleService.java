package fr.epsi.REST.service;


import fr.epsi.REST.model.Article;

import java.util.List;

public interface ArticleService {

    List<Article>getAll();

   Article getById(Integer id);

   Article create(Article article);

   Article update(Article article);

   void delete(Integer id);
}

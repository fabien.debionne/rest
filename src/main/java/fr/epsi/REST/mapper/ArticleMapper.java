package fr.epsi.REST.mapper;

import fr.epsi.REST.api.dto.ArticleDto;
import fr.epsi.REST.model.Article;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel ="spring")
public interface ArticleMapper {
    ArticleDto mapToDto(Article article);

    Article mapToModel(ArticleDto articleDto);
}

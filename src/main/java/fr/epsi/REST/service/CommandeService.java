package fr.epsi.REST.service;

import fr.epsi.REST.model.Commande;

import java.util.List;

public interface CommandeService {

    List<Commande> getAll();

    Commande getById(Integer id);

    Commande create(Commande commande);

    Commande update(Commande commande);

    void delete(Integer id);
}

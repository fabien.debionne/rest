package fr.epsi.REST.service.impl;

import fr.epsi.REST.exception.UnknowRessourceException;
import fr.epsi.REST.model.Article;
import fr.epsi.REST.model.Commande;
import fr.epsi.REST.repository.CommandeRepository;
import fr.epsi.REST.service.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandeServiceImpl implements CommandeService {

    @Autowired
    private CommandeRepository commandeRepository;

    @Override
    public List<Commande> getAll() {
        return commandeRepository.findAll(Sort.by("id").ascending());
    }

    @Override
    public Commande getById(Integer id) {
        return commandeRepository.findById(id).
                orElseThrow(() -> new UnknowRessourceException("Pas de commande trouvé pour cet identifiant"));
    }

    @Override
    public Commande create(Commande commande) {
        for (Article article : commande.getArticles()) {
            article.setQuantite(article.getQuantite()-1);
        }
        return commandeRepository.save(commande);
    }

    @Override
    public Commande update(Commande commande) {
        Commande existingCommande = this.getById(commande.getId());
        List<Article> articles = existingCommande.getArticles();
        for (Article article : articles) {
            article.setQuantite(article.getQuantite()-1);
        }
        existingCommande.setArticles(commande.getArticles());
        return commandeRepository.save(existingCommande);
    }

    @Override
    public void delete(Integer id) {
        Commande commandeToDelete = this.getById(id);
        commandeRepository.delete(commandeToDelete);
    }
}

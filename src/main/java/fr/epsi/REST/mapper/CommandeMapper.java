package fr.epsi.REST.mapper;

import fr.epsi.REST.api.dto.ArticleDto;
import fr.epsi.REST.api.dto.CommandeDto;
import fr.epsi.REST.model.Commande;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper
public interface CommandeMapper {

    CommandeDto mapToDto(Commande commande);

    Commande mapToModel(CommandeDto commandeDto);

    default List<ArticleDto> getArticles(Commande commande){
        if(commande.getArticles() != null){
            return commande.getArticles().stream()
                    .map(article -> new ArticleDto(
                            article.getId(),
                            article.getDesignation(),
                            article.getPrix(),
                            article.getQuantite()
                                        ))
                    .toList();
        }
        return new ArrayList<>();
    }
}


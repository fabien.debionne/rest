package fr.epsi.REST.api.v1;

import fr.epsi.REST.api.dto.ArticleDto;
import fr.epsi.REST.exception.UnknowRessourceException;
import fr.epsi.REST.mapper.ArticleMapper;
import fr.epsi.REST.service.ArticleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/articles")
@CrossOrigin(value= {"*"}, allowedHeaders = {"*"})
public class ArticleApi {

    private ArticleService articleService;

    private ArticleMapper articleMapper;

    public  ArticleApi(ArticleService articleService, ArticleMapper articleMapper){
        this.articleService = articleService;
        this.articleMapper =articleMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Retourne la liste de tous les articles", responses = @ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ArticleDto.class)))))
    public ResponseEntity<List<ArticleDto>> getAll(){
        return ResponseEntity
                .ok(articleService.getAll().stream()
                        .map(article -> this.articleMapper.mapToDto(article))
                        .collect(Collectors.toList()));
    }


    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
                 consumes = {MediaType.APPLICATION_JSON_VALUE,
                 MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "Création de l'article",
               responses = @ApiResponse(responseCode = "201", description = "Article créé"))
    public ResponseEntity<ArticleDto> create(@RequestBody final ArticleDto articleDto){
        ArticleDto newArticle = articleMapper
                .mapToDto(articleService
                        .create(articleMapper.mapToModel(articleDto)));
        return ResponseEntity.created(URI.create("/articles/" + newArticle.getId())).body(newArticle);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Suppression de l'article", responses = @ApiResponse(responseCode = "204", description = "Article supprimé"))
    public ResponseEntity<Void>delete(@PathVariable final Integer id){
        try {
            articleService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (UnknowRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "article non trouvé");
        }
    }

    @PutMapping(path = "/{id}",
                produces = { MediaType.APPLICATION_JSON_VALUE},
                consumes = { MediaType.APPLICATION_JSON_VALUE,
                             MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "mise à jour article", responses = @ApiResponse(responseCode = "204", description = "Pas de contenu"))
    public ResponseEntity<Void> update(@PathVariable final Integer id, @RequestBody ArticleDto articleDto) {
        try{
            articleDto.setId(id);
            articleService.update(articleMapper.mapToModel(articleDto));
            return ResponseEntity.noContent().build();
        } catch (UnknowRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "article non trouvé");
        }
    }

}

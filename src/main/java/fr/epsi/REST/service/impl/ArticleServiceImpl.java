package fr.epsi.REST.service.impl;

import fr.epsi.REST.exception.UnknowRessourceException;
import fr.epsi.REST.model.Article;
import fr.epsi.REST.repository.ArticleRepository;
import fr.epsi.REST.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService
{
    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public List<Article> getAll() {
        return articleRepository.findAll(Sort.by("designation").ascending());
    }

    @Override
    public Article getById(Integer id) {
        return articleRepository.findById(id)
                .orElseThrow(() -> new UnknowRessourceException("Pas d'article trouvé pour cet identifiant"));
    }

    @Override
    public Article create(Article article) {
        return articleRepository.save(article);
    }

    @Override
    public Article update(Article article) {
        Article existingArticle = this.getById(article.getId());
        existingArticle.setDesignation(article.getDesignation());
        existingArticle.setPrix(article.getPrix());
        existingArticle.setQuantite(article.getQuantite());
        return articleRepository.save(existingArticle);
    }

    @Override
    public void delete(Integer id) {
        Article articleToDelete = this.getById(id);
        articleRepository.delete(articleToDelete);
    }
}
